var config = module.exports = {};

//app
config.app = {};
config.app.port = 3001;

//mongodb
config.mongo = {};
config.mongo.uri = '127.0.0.1';
config.mongo.db = 'movies';
